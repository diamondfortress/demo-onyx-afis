import {AlertController} from "ionic-angular";
import {TranslateService} from 'ng2-translate';
import {Constants} from "../../../../both/Constants";

declare var Meteor;
declare var navigator;
declare var Onyx;

export class OnyxHelper {
    constructor(private alertCtrl:AlertController, 
                private translate:TranslateService, 
                private callback) {
    }

    public execOnyx(options) {
        var self = this;
        if (Meteor.isCordova) {
            options.onyxLicense = Meteor.settings.public["onyxLicense"];
            navigator.onyx.exec(
                options,
                this.successCallback.bind(this),
                this.errorCallback.bind(this)
            );
        } else {
            console.log("This feature is only available on cordova devices.");
            let alert = self.alertCtrl.create({
                title: "Cordova Only!",
                message: "This feature is only available on cordova devices.",
                buttons: ["OK"]
            });
            alert.present();
        }
    }

    private successCallback(result):void {
        console.log("successCallback(): " + JSON.stringify(result));
        console.log("action: " + result.action);
        this.callback(result);
    }

    private errorCallback(message):void {
        var self = this;
        console.log("errorCallback(): " + message);
        if (message !== "Cancelled") {
            let alert = self.alertCtrl.create({
                title: "Onyx Error",
                message: message,
                buttons: ["OK"]
            });
            alert.present();
        }
    }
}