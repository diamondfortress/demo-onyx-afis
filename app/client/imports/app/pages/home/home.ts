import {Component, OnInit, NgZone} from '@angular/core';
import {App, NavController, AlertController} from 'ionic-angular';
import {MeteorComponent} from 'angular2-meteor';
import {TranslateService} from 'ng2-translate';
import {Constants} from "../../../../../both/Constants";
import {OnyxHelper} from "../../utils/OnyxHelper";

declare var Onyx; // cordova-plugin-onyx
declare var device; // cordova-plugin-device

import template from './home.html';
@Component({
    selector: 'page-home',
    template
})
export class HomePage extends MeteorComponent implements OnInit {
    public user:Meteor.User;
    private onyxHelper:OnyxHelper;
    public ONYX_ACTIONS:any = Constants.ONYX_ACTIONS;

    constructor(public app:App,
                public nav:NavController,
                public alertCtrl:AlertController,
                public zone:NgZone,
                public translate:TranslateService) {
        super();
    }

    ngOnInit() {
        this.onyxHelper = new OnyxHelper(this.alertCtrl, this.translate, this.onyxCallback.bind(this));
        // Use MeteorComponent autorun to respond to reactive session variables.
        this.autorun(() => this.zone.run(() => {
            this.user = Meteor.user();

            // Wait for translations to be ready
            // in case component loads before the language is set
            // or the language is changed after the component has been rendered.
            // Since this is the home page, this component and any child components
            // will need to wait for translations to be ready.
            if (Session.get(Constants.SESSION.TRANSLATIONS_READY)) {
                this.translate.get('page-home.title').subscribe((translation:string) => {

                    // Set title of web page in browser
                    this.app.setTitle(translation);
                });
            }
        }));
    }

    private capture(action:string):void {
        if (Meteor.isCordova) {
            Session.set(Constants.SESSION.ONYX_ACTION, action);
            this.onyxHelper.execOnyx({
                action: Onyx.Action.IMAGE,
                imageTypes: [
                    Onyx.ImageType.WSQ
                ]
            });
        }
    }

    private onyxCallback(result:any):void {
        var self = this;
        Session.set(Constants.SESSION.LOADING, true);
        switch (result.action) {
            case Onyx.Action.IMAGE:
                if (result.images.hasOwnProperty(Onyx.ImageType.WSQ)) {
                    console.log("images contains WSQ image");
                    var wsqImage:any = result.images[Onyx.ImageType.WSQ];
                    if (wsqImage) {
                        console.log("wsqImageNfiqScore: " + wsqImage.nfiqScore);
                        if (wsqImage.nfiqScore > 0 && wsqImage.nfiqScore < 5) {
                            switch (Session.get(Constants.SESSION.ONYX_ACTION)) {
                                case Constants.ONYX_ACTIONS.ENROLL:
                                    self.enrollWsqImage(wsqImage.bytes);
                                    break;
                                case Constants.ONYX_ACTIONS.VERIFY:
                                    self.vectorVerifyWsqImage(wsqImage.bytes);
                                    break;
                                case Constants.ONYX_ACTIONS.IDENTIFY:
                                    self.identifyWsqImage(wsqImage.bytes);
                                    break;
                            }
                        } else {
                            Session.set(Constants.SESSION.LOADING, false);
                            let alert = self.alertCtrl.create({
                                title: "Poor Image Quality",
                                message: "NFIQ Score: " + wsqImage.nfiqScore,
                                buttons: ["OK"]
                            });
                            alert.present()
                        }
                    }
                }
                break;
        }
    }

    private enrollWsqImage(wsqImageUri:string):void {
        var self = this;
        Meteor.call('onyx/fingerprint/wsq/save', {
            wsqImage: wsqImageUri,
            device: device
        }, (error, saveResult) => {
            Session.set(Constants.SESSION.LOADING, false);
            if (error) {
                console.log("Error enrolling wsq image: " + JSON.stringify(error));
                let alert = self.alertCtrl.create({
                    title: "Enroll Error",
                    message: JSON.stringify(error),
                    buttons: ["OK"]
                });
                alert.present();
            } else {
                console.log("save result: " + JSON.stringify(saveResult));
                var saveResultMessage = saveResult.message;
                if (saveResult.success) {
                    console.log("Successfully enroll fingerprint.");
                    saveResultMessage += "<br><br>Enrolled new fingerprint ID: " + saveResult.fingerprintId;
                } else if (!saveResult.success && saveResult.fingerprintId) {
                    saveResultMessage += "<br><br>Enrolled existing fingerprint ID: " + saveResult.fingerprintId;
                }
                let alert = self.alertCtrl.create({
                    title: "Enroll Result",
                    message: saveResultMessage,
                    buttons: ["OK"]
                });
                alert.present();
            }
        });
    }

    private vectorVerifyWsqImage(wsqImageUri:string):void {
        var self = this;
        Meteor.call('onyx/fingerprint/wsq/vector', {
            wsqImage: wsqImageUri,
            device: device
        }, (error, verifyResult) => {
            Session.set(Constants.SESSION.LOADING, false);
            if (error) {
                console.log("Error verifying wsq image: " + JSON.stringify(error));
                let alert = self.alertCtrl.create({
                    title: "Verify Error",
                    message: JSON.stringify(error),
                    buttons: ["OK"]
                });
                alert.present();
            } else {
                console.log("verify result: " + JSON.stringify(verifyResult));
                var verifyResultMessage = verifyResult.message;
                if (verifyResult.success) {
                    console.log("Successfully verified fingerprint.");
                    verifyResultMessage += "<br><br>Matched fingerprint ID: " + verifyResult.fingerprintId;
                    verifyResultMessage += "<br><br>Match score: " + verifyResult.score;
                } 
                let alert = self.alertCtrl.create({
                    title: "Verify Result",
                    message: verifyResultMessage,
                    buttons: ["OK"]
                });
                alert.present();
            }
        });
    }

    private identifyWsqImage(wsqImageUri:string):void {
        var self = this;
        Meteor.call('onyx/fingerprint/wsq/identify', {
            wsqImage: wsqImageUri,
            device: device
        }, (error, identifyResult) => {
            Session.set(Constants.SESSION.LOADING, false);
            if (error) {
                console.log("Error identifying wsq image: " + JSON.stringify(error));
                let alert = self.alertCtrl.create({
                    title: "Identify Error",
                    message: JSON.stringify(error),
                    buttons: ["OK"]
                });
                alert.present();
            } else {
                console.log("identify result: " + JSON.stringify(identifyResult));
                var identifyResultMessage = identifyResult.message;
                if (identifyResult.success) {
                    console.log("Successfully verified fingerprint.");
                    identifyResultMessage += "<br><br>Matched fingerprint ID: " + identifyResult.fingerprintId;
                    identifyResultMessage += "<br><br>Match score: " + identifyResult.score;
                }
                let alert = self.alertCtrl.create({
                    title: "Identify Result",
                    message: identifyResultMessage,
                    buttons: ["OK"]
                });
                alert.present()
            }
        });
    }
}
