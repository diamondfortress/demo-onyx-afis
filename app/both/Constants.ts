export class Constants {
    public static EMPTY_STRING = "";

    public static SESSION:any = {
        LANGUAGE: "language",
        LOADING: "isLoading",
        PLATFORM_READY: "platformReady",
        TRANSLATIONS_READY: "translationsReady",
        PATH: "path",
        URL_PARAMS: "urlParams",
        INCORRECT_PASSWORD: "incorrectPassword",
        FORGOT_PASSWORD: "forgotPassword",
        CREATE_ACCOUNT: "createAccount",
        RESET_PASSWORD: "resetPassword",
        REGISTERED_ERROR: "registeredError",
        NOT_REGISTERED_ERROR: "notRegisteredError",
        RESET_PASSWORD_ERROR: "resetPasswordError",
        RESET_PASSWORD_ERROR_MESSAGE: "resetPasswordErrorMessage",
        RESET_PASSWORD_TOKEN: "resetPasswordToken",
        WAS_PASSWORD_RESET: "wasPasswordReset",
        EMAIL: "email",
        ONYX_ACTION: "onyxAction"
    };

    public static DEVICE:any = {
        IOS: "iOS",
        ANDROID: "Android"
    };

    public static STYLE:any = {
        IOS: "ios",
        MD: "md"
    };

    public static METEOR_ERRORS:any = {
        SIGN_IN: "sign-in",
        ACCOUNT_NOT_FOUND: "account-not-found",
        NO_PASSWORD: "User has no password set",
        USER_NOT_FOUND: "User not found",
        INCORRECT_PASSWORD: "Incorrect password",
        EMAIL_EXISTS: "Email already exists.",
        TOKEN_EXPIRED: "Token expired",
        TIMEDOUT: "ETIMEDOUT"
    };

    public static ADD_IMAGE_PLACEHOLDER_URI:string = "/images/add_image_camera_photo.png";
    public static IMAGE_URI_PREFIX:string = "data:image/jpeg;base64,";

    public static ONYX_AFIS_ENDPOINTS:any = {
        ENROLL: "/api/v1/onyx/enroll",
        VECTOR: "/api/v1/onyx/vector",
        WSQ: {
            ENROLL: "/api/v1/onyx/wsq/enroll",
            IDENTIFY: "/api/v1/onyx/wsq/identify",
            VECTOR: "/api/v1/onyx/wsq/vector"
        }
    };

    public static ONYX_ACTIONS:any = {
        ENROLL: "enroll",
        IDENTIFY: "identify",
        VERIFY: "verify"
    };
}