import {Constants} from "../../../both/Constants";
var Future = Npm.require('fibers/future');

declare var console;
declare var Accounts;

export class MeteorMethods {

    public init():void {
        Meteor.methods({
            'sample': () => {
                var user:Meteor.User = MeteorMethods.checkForUser();  // throws errors
                if (user) {
                    var future:any = new Future();
                    try {
                        return future.wait();
                    } catch (error) {
                        throw error;
                    }
                }
            },
            'onyx/fingerprint/wsq/save': (data:{wsqImage:string, device:any}) => {
                var user:Meteor.User = MeteorMethods.checkForUser();  // throws errors
                if (user) {
                    var future:any = new Future();
                    var onyxAfisEndpoint:string = Meteor.settings.private["afis"]["basePath"] +
                        Constants.ONYX_AFIS_ENDPOINTS.WSQ.ENROLL;
                    console.log("onyxAfisEndpoint: ", onyxAfisEndpoint);
                    var postData:any = {
                        wsqImage: data.wsqImage,
                        api_key: Meteor.settings.private["afis"]["apiKey"],
                        device: data.device
                    };
                    HTTP.call("POST", onyxAfisEndpoint, {
                        data: postData,
                        timeout: 20 * 1000
                    }, function (error:any, result:any) {
                        if (error) {
                            future.throw(error)
                        } else {
                            console.log("result.data: ", result.data);
                            if (result.data.fingerprintId) {
                                var fingerprintId:string = result.data.fingerprintId;
                                // Store fingerprintId
                                var fingerprintIds:Array<string> = user["fingerprintIds"] || new Array();
                                var fingerprintIdIndex:number = fingerprintIds.indexOf(fingerprintId);
                                console.log("fingerprintIdIndex: ", fingerprintIdIndex);
                                if (fingerprintIdIndex === -1) {
                                    console.log("Adding fingerprintId");
                                    fingerprintIds.push(fingerprintId);
                                    user["fingerprintIds"] = fingerprintIds;
                                } else {
                                    console.log("FingerprintId already enrolled");
                                }

                                Meteor.users.update({_id: user._id}, user, (error, updateResult) => {
                                    if (error) {
                                        future.throw(error);
                                    } else {
                                        future.return(result.data);
                                    }
                                });
                            } else {
                                future.return(result.data);
                            }
                        }
                    });
                    try {
                        return future.wait();
                    } catch (error) {
                        console.log("Onyx AFIS Error: ", error);
                        if (error.code === Constants.METEOR_ERRORS.TIMEDOUT) {
                            error = new Meteor.Error(Constants.METEOR_ERRORS.TIMEDOUT);
                        }
                        throw error;
                    }
                }
            },
            'onyx/fingerprint/wsq/vector': (data:{wsqImage:string, device:any}) => {
                var user:Meteor.User = MeteorMethods.checkForUser();  // throws errors
                if (user) {
                    var future:any = new Future();
                    var onyxAfisEndpoint:string = Meteor.settings.private["afis"]["basePath"] +
                        Constants.ONYX_AFIS_ENDPOINTS.WSQ.VECTOR;
                    console.log("onyxAfisEndpoint: ", onyxAfisEndpoint);
                    var postData:any = {
                        wsqImage: data.wsqImage,
                        api_key: Meteor.settings.private["afis"]["apiKey"],
                        device: data.device,
                        fingerprintIds: user["fingerprintIds"]
                    };
                    HTTP.call("POST", onyxAfisEndpoint, {
                        data: postData,
                        timeout: 20 * 1000
                    }, function (error:any, result:any) {
                        if (error) {
                            future.throw(error)
                        } else {
                            console.log("result.data: ", result.data);
                            future.return(result.data);
                        }
                    });
                    try {
                        return future.wait();
                    } catch (error) {
                        console.log("Onyx AFIS Error: ", error);
                        if (error.code === Constants.METEOR_ERRORS.TIMEDOUT) {
                            error = new Meteor.Error(Constants.METEOR_ERRORS.TIMEDOUT);
                        }
                        throw error;
                    }
                }
            },
            'onyx/fingerprint/wsq/identify': (data:{wsqImage:string, device:any}) => {
                var user:Meteor.User = MeteorMethods.checkForUser();  // throws errors
                if (user) {
                    var future:any = new Future();
                    var onyxAfisEndpoint:string = Meteor.settings.private["afis"]["basePath"] +
                        Constants.ONYX_AFIS_ENDPOINTS.WSQ.IDENTIFY;
                    console.log("onyxAfisEndpoint: ", onyxAfisEndpoint);
                    var postData:any = {
                        wsqImage: data.wsqImage,
                        api_key: Meteor.settings.private["afis"]["apiKey"],
                        device: data.device
                    };
                    HTTP.call("POST", onyxAfisEndpoint, {
                        data: postData,
                        timeout: 20 * 1000
                    }, function (error:any, result:any) {
                        if (error) {
                            future.throw(error)
                        } else {
                            console.log("result.data: ", result.data);
                            future.return(result.data);
                        }
                    });
                    try {
                        return future.wait();
                    } catch (error) {
                        console.log("Onyx AFIS Error: ", error);
                        if (error.code === Constants.METEOR_ERRORS.TIMEDOUT) {
                            error = new Meteor.Error(Constants.METEOR_ERRORS.TIMEDOUT);
                        }
                        throw error;
                    }
                }
            }
        });
    }

    public static checkForUser():Meteor.User {
        var currentUserId = Meteor.userId();
        var user:Meteor.User;
        if (!currentUserId) {
            throw new Meteor.Error(Constants.METEOR_ERRORS.SIGN_IN, "Please sign in.");
        } else {
            user = Meteor.users.findOne(currentUserId);
            if (!user) {
                throw new Meteor.Error(Constants.METEOR_ERRORS.ACCOUNT_NOT_FOUND, "Invalid User ID");
            }
        }
        return user;
    }
}